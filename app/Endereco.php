<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $fillable = [
        'logradouro', 'bairro', 'cidade', 'uf', 'cep', 'numero', 'complemento',
    ];

    public function usuario()
    {
        return $this->hasOne('App\Usuario');
        
    }

    public function Empresa()
    {
        return $this->hasOne('App\Empresa');
    }
}
