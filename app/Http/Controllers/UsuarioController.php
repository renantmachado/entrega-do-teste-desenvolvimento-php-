<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\Http\Resources\UsuarioResource; 
use App\Http\Resources\UsuariosCollection;

class UsuarioController extends Controller
{
    public function index(){
    	
	return new UsuariosCollection(Usuario::all()); 

    }
    public function show($id){
    	$usuario = Usuario::find($id);
    	if($usuario){
	    	return new UsuarioResource($usuario);
    	}else{
    		return response()->json(['data' => 'Resource not found'], 400);
    	}
    }
    public function store(Request $request){

        $usuario = Usuario::create($request->all());

        return response()->json($usuario, 201);
        
    }

    public function update(Request $request, $id){
    	$usuario = Usuario::find($id);
    	
    	if($usuario){
    	    $usuario->update($request->all());
        return response()->json($usuario, 200);
    }else {
		return response()->json(['data' => 'Resource not found'], 400);    }
    }

    public function delete($id){

    	$usuario = Usuario::find($id);

    	if($usuario){
	        $usuario->delete();
	        return response()->json(null, 204);
			    }else{
		    	return response()->json(['data' => 'Resource not found'], 400);  	
		    	}
    }	



}
