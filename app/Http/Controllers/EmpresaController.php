<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\EmpresaResource; 
use App\Http\Resources\EmpresasCollection;
use App\Empresa;


class EmpresaController extends Controller
{
   public function index(){
    	
	return new EmpresasCollection(Empresa::all()); 

    }
    
    public function show($id){
    	$empresa = Empresa::find($id);
    	if($empresa){
	    	return new EmpresaResource($empresa);
    	}else{
    		return response()->json(['data' => 'Resource not found'], 400);
    	}
    }

    public function store(Request $request){

        $empresa = Empresa::create($request->all());

        return response()->json($empresa, 201);
        //return $request->all();
    }

    public function update(Request $request, $id)    {
		$empresa = Empresa::find($id);
    	if($empresa){   
        $empresa->update($request->all());
        return response()->json($empresa, 200);
	    }else{
	    	return response()->json(['data' => 'Resource not found'], 400);
	    }
    }
    public function delete($id){
        $empresa = Empresa::find($id);
    	if($empresa){  
        	$empresa->delete();
        	return response()->json(null, 204);
	    }else{
	    	return response()->json(['data' => 'Resource not found'], 400);
	    }
    }


}
