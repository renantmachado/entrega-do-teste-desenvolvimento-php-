<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsuarioResource extends JsonResource
{
      public $preserveKeys = true;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
                'id' => $this->id,
                'nome' => $this->nome,
                'cpf' => $this->cpf,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'empresas' => new EmpresasCollection($this->empresas),
                'endereco' => new EnderecoResource($this->endereco),
                'links' => [
                'url' => route('show_usuario', ['id' => $this->id])
            ]
        ];
                
    }
}
            