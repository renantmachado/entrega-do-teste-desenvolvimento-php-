<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmpresaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
                'id' => $this->id,
                'nome' => $this->nome,
                'cnpj' => $this->cnpj,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'usuarios' => new UsuariosCollection($this->usuarios),
                'endereco' => new EnderecoResource($this->endereco),
                'links' => [
                'url' => route('show_empresa', ['id' => $this->id])
                 ]              
            ]; 
    }
}
