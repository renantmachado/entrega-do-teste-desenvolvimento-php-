<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = [
        'nome', 'cnpj', 'endereco_id',
    ];

    public function usuarios()
    {
        return $this->belongsToMany(Usuario::class, 'empresa_usuario');
    }

    public function endereco()
    {
        return $this->belongsTo('App\Endereco');
    }

}
