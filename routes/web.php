<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/usuarios', 'UsuarioController@index')->name('index_usuario');
Route::get('/usuarios/{id}', 'UsuarioController@show')->name('show_usuario');
Route::post('/usuarios', 'UsuarioController@store')->name('store_usuario');
Route::put('/usuarios/{id}', 'UsuarioController@update')->name('update_usuario');
Route::delete('usuarios/{id}', 'UsuarioController@delete')->name('delete_usuario');
Route::get('/empresas', 'EmpresaController@index')->name('index_empresa');
Route::get('/empresas/{id}', 'EmpresaController@show')->name('show_empresa');
Route::post('/empresas', 'EmpresaController@store')->name('store_empresa');
Route::put('/empresas/{id}', 'EmpresaController@update')->name('update_empresa');
Route::delete('empresas/{id}', 'EmpresaController@delete')->name('delete_empresa');