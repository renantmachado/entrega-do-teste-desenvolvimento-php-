<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Usuario;
use App\Endereco;
use App\Empresa;

class UsuarioTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testCreateUsuario(){
        $end = Endereco::create([
            'logradouro' => 'rua morro das feiticeiras',
             'bairro' =>'Ingleses',
             'cidade'=> 'Florianópolis', 
             'uf' => 'SC',
             'cep' => '88058-583',
             'numero' => '495',
             'complemento'=> 'apto 201 bloco B'
        ]); 
        $usu = Usuario::create([
            'nome' => 'Renan Tabares Machado',
            'cpf' => '81780745087', 
            'email' => 'renantabares@gmail.com',
            'password' => bcrypt (123456),
            'endereco_id' => $end->id
        ]);

         $emp = Empresa::where('cnpj', '78790617000145')->first();
         $usu->empresas()->save($emp);
         $usu->save();

        $this->assertDatabaseHas('usuarios', [
            'cpf' => '81780745087'
            ]);

    }
}