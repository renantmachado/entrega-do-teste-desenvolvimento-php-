<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Endereco;
use App\Empresa; 

class EmpresaTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    public function testCreateEmpresa(){
        $end = Endereco::create([
            'logradouro' => 'Rua Lauro Linhares',
             'bairro' =>'Trindade',
             'cidade'=> 'Florianópolis', 
             'uf' => 'SC',
             'cep' => '88036-002',
             'numero' => '2123',
             'complemento'=> 'Torre B, 3º andar'
        ]); 

        $emp = Empresa::create([
            'nome' => 'BRy Tecnologia',
            'cnpj' => '78790617000145',
            'endereco_id' => $end->id
        ]);

            
        $this->assertDatabaseHas('empresas', [
            'cnpj' => '78790617000145'
            ]);
    }

}
