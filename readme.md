<p align="center"> desenvolvido com<img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
Entrega do Teste para desenvolvedor PHP BRY<br>
Renan Tabares Machado, 
</p>



## Recursos


- **[Laravel](https://laravel.com/)**
- **[composer](https://getcomposer.org/)**
- **[Postgres](https://www.postgresql.org/)**
- **[Postman](https://www.getpostman.com/)**
- **[Git](https://git-scm.com/)**

## Passos para rodar a aplicação

- **[1-Clonar esse repositório]**
- **[2-No diretório raiz, rodar no seguinte comando no terminal:]**
- **[2.1-php artisan migrate]**
- **[2.2-rodar os teste]**
- **[2.3-vendor/bin/phpunit]**
- **[2.4-php artisan serve]**
- **[3-aguardar a mensagem:]**
- **[Laravel development server started: <http://127.0.0.1:8000>]**

## passos para executar os testes:

- **[1-Abrir o Postman]**
- **[2-Executar as seguintes requisições]**
- **[2.1 Listar todos usuários - GET:localhost:8000/usuarios(localhost:8000/usuarios)]**
- **[2.2 Usuário por id - GET:localhost:8000/usuarios/1(localhost:8000/usuarios/1)]**
- **[2.3 Cadastrar um novo usuário - POST:localhost:8000/usuarios - colocando no corpo da requisição]**
- **[{
	"nome": "João da Silva",
    "cpf": "91890845089",
    "email": "joao@email.com",
    "password": "123456",
    "endereco_id": 1
}]**
- **[2.4 Editar um usuário - PUT:localhost:8000/usuarios/{ID} - colocando no corpo da requisição]**
- **[{
	"nome": "João da Silva Santos",
    "cpf": "91890845089",
    "email": "joao@email.com",
    "password": "123456",
    "endereco_id": 1
}]**
- **[2.5 Excluir usuário  - DELETE:localhost:8000/usuarios/{ID}]**

- **[2.6 Listar todas empresas - GET:localhost:8000/empresas(localhost:8000/empresas)]**
- **[2.7 Empresa por id - GET:localhost:8000/empresas/1(localhost:8000/empresas/1)]**
- **[2.8 Cadastrar um nova Empresa - POST:localhost:8000/empresas - colocando no corpo da requisição]**
- **[{
	"nome": "Angeloni",
    "cnpj": "28824045000108",
    "endereco_id": 2
}]**
- **[2.9 Editar um Empresa - PUT:localhost:8000/empresas/{ID} - colocando no corpo da requisição]**
- **[{
	 "nome": "Angeloni Supermercado",
    "cnpj": "28824045000108",
    "endereco_id": 2
}]**
- **[2.10 Excluir empresa  - DELETE:localhost:8000/empresas/{ID}]**



